#include <assert.h>
#include <dlfcn.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <wayland-server-core.h>

#define MAX_LINKS 512

static void (*original_wl_signal_emit_mutable)(struct wl_signal *signal, void *data) = NULL;
static int urandom = -1;

static void init(void) {
	static bool initialized = false;
	if (initialized) {
		return;
	}
	original_wl_signal_emit_mutable = dlsym(RTLD_NEXT, "wl_signal_emit_mutable");
	urandom = open("/dev/urandom", O_RDONLY | O_CLOEXEC);
	initialized = true;
	fputs("[wlshuffle] Initialized!\n", stderr);
}

// [0…max]
static inline int rnd(int max) {
	union {
		uint32_t u;
		char b[4];
	} conv;
	read(urandom, conv.b, sizeof(conv.b));
	return (float)conv.u / UINT32_MAX * max + 0.5f;
}

void wl_signal_emit_mutable(struct wl_signal *signal, void *data) {
	init();

	struct wl_list *list = &signal->listener_list;

	static struct wl_list *links[MAX_LINKS];
	int length = 0;
	for (struct wl_list *iter = list->next; iter != list; iter = iter->next) {
		assert(length != MAX_LINKS);
		links[length++] = iter;
	}

	for (int i = length - 1; i > 0; i--) {
		int j = rnd(i);
		struct wl_list *tmp = links[j];
		links[j] = links[i];
		links[i] = tmp;
	}

	wl_list_init(list);
	for (int i = 0; i < length; i++) {
		wl_list_insert(list->prev, links[i]);
	}

	original_wl_signal_emit_mutable(signal, data);
}
