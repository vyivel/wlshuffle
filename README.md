# wlshuffle

A library to randomly shuffle the listeners when `wl_signal_emit_mutable()` is called.

## Building

```sh
meson setup build/
ninja -C build/
```

## Usage

```sh
LD_PRELOAD=libwlshuffle.so path/to/program
```

## FQA

> Why?

To check whether a program relies on a specific listener order.

> No `wl_signal_emit()`?

That one's an inline function. Sad.

> Why `/dev/urandom` and not `rand()`?

To avoid touching the global state. Also `rand()` is Bad.

## License

BSD-2-Clause

See `LICENSE` for more information.
